class PplanAdapter < ConfigAdapter
  def initialize(fields, source)
    super
    @doc.remove_namespaces!
  end

  self.register_class /pplan/
end
