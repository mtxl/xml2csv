require "xml2csv/version"
require 'xml2csv/adapters/base_adapter'
require 'xml2csv/adapters/config_adapter'
require 'xml2csv/adapters/pplan_adapter'
require 'yaml'
require 'csv'
require 'nokogiri'
require 'slop'
require 'byebug'

module Xml2Csv

  def self.convert_csv(config, source, xml_dir, params ={} )
    output_dir = params[:output] || "output"
    header = params[:header] || false
    single = params[:single] || false

    parsed = BaseAdapter.instance(config, source)

    csv_file = xml_dir && single ? File.join(output_dir, xml_dir) + ".csv" : File.join(output_dir,File.basename(source, ".xml"))+".csv"
    is_csv_new = File.exist? csv_file

    CSV.open(csv_file, "ab") do |csv|
      csv << parsed.header if header && !is_csv_new
      parsed.each_item do |data|
        puts data if @opts.verbose?
        csv << data
      end
    end
  end

  def self.process
    opts = Slop.parse(help: true) do 
      banner "Usage: xml2csv [options] xml_source"
      banner "       xml_source may be file or directory"
      on :c, :config=, 'config'
      on :t, :header,  'csv with names header'
      on :o, :output=, 'output directory'
      on :s, :single,  'single file'
      on :v, :verbose, 'verbose mode'
    end

    unless opts.config? && File.exist?(opts[:config])
      puts "You mast select config file!"
      puts opts.help
      exit
    end
    @opts = opts

    xml_source = ARGV.shift
    unless xml_source
      puts "You must select xml file!"
      exit
    else
      if File.directory?(xml_source)
        xml_dir = xml_source.split("/").last
        xml_source = Dir.glob("#{xml_source}/*.xml")
      else
        xml_source = [xml_source]
      end
    end

    xml_source.each do |source_file|
      puts source_file if opts.verbose?
      convert_csv(opts[:config], source_file, xml_dir, opts.to_hash)
    end

  end
end
