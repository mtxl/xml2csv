unzip_all () {
  mkdir -p 'xml/'$region
  find $1 -name '*.zip' -print | xargs -I {} unzip {} '*.xml' -d 'xml/'$region
}

get_plans () {
  for region in $(cat $1)
  do
    acc='purchaseplan_'$region'_2019040100_2019050100_*.xml.zip'
    path='ftp.zakupki.gov.ru/fcs_regions/'$region'/purchaseplans/'
    url='ftp://'$path
    wget --user=free --password=free -r --no-parent -A $acc $url
    unzip_all $path $reion
    bin/xml2csv -s -t -o out -c config/pp_full.yml 'xml/'$region
    rm -r xml
    rm -r 'ftp.zakupki.gov.ru'
  done
}
get_plans list
